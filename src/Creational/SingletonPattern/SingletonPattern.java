package Creational.SingletonPattern;

public class SingletonPattern {

    private SingletonPattern instance;

    private SingletonPattern() {}

    public SingletonPattern getInstance() {
        if (instance == null)
            instance = new SingletonPattern();
        return instance;
    }

}
