package Creational.DependencyInjection;

public class Client {

    Action action;

    public Client(Action action) {
        this.action = action;
    }

    public void Act() {
        action.execute();
    }

}
