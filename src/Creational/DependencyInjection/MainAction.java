package Creational.DependencyInjection;

public class MainAction implements Action {
    @Override
    public void execute() {
        System.out.println("Executed");
    }
}
