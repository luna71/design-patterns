package Creational.DependencyInjection;

public interface Action {
    void execute();
}
