package Creational.FactoryPattern;

import Creational.FactoryPattern.Shapes.Circle;
import Creational.FactoryPattern.Shapes.Shape;
import Creational.FactoryPattern.Shapes.Square;
import Creational.FactoryPattern.Shapes.Triangle;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {

    private static Map<Integer, Shape> Shapes = new HashMap();

    static { // Still violates OCP, can't figure out how to have these classes self register reliably without instantiating them first
        registerShape(new Circle(), Circle.sides);
        registerShape(new Triangle(), Triangle.sides);
        registerShape(new Square(), Square.sides);
    }

    public static void registerShape(Shape shape, Integer sides) {
        Shapes.put(sides, shape);
    }

    public static Shape makeShape(Integer sides) {
        return Shapes.get(sides);
    }

}
