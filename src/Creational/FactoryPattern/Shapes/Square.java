package Creational.FactoryPattern.Shapes;

public class Square extends Shape {

    public static final Integer sides = 4;

    @Override
    public void Draw() {
        System.out.println("Drawn Square");
    }

    @Override
    Shape createShape() {
        return new Square();
    }
}
