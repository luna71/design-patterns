package Creational.FactoryPattern.Shapes;

public class Triangle extends Shape {

    public static final Integer sides = 3;

    @Override
    public void Draw() {
        System.out.println("Drawn Triangle");
    }

    @Override
    Shape createShape() {
        return new Triangle();
    }
}
