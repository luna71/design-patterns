package Creational.FactoryPattern.Shapes;

public class Circle extends Shape{

    public static final Integer sides = 1;

    @Override
    public void Draw() {
        System.out.println("Drawn Circle");
    }

    @Override
    Shape createShape() {
        return new Circle();
    }
}
