package Creational.FactoryPattern.Shapes;

public abstract class Shape {

    public abstract void Draw();

    abstract Shape createShape();


}
