package Creational.AbstractFactoryPattern;

import java.awt.*;

public class UIButton implements Button {

    Color buttonColor;

    @Override
    public void Click() {
        System.out.println("Clicked");
    }

    @Override
    public void SetColor(Color color) {
        buttonColor = color;
    }
}
