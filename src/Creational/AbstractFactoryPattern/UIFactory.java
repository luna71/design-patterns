package Creational.AbstractFactoryPattern;

public interface UIFactory {
    public Button createButton();
}
