package Creational.AbstractFactoryPattern;

import java.awt.*;

public class DarkUIFactory implements UIFactory {
    @Override
    public Button createButton() {
        Button uiButton = new UIButton();
        uiButton.SetColor(Color.BLACK);
        return uiButton;
    }
}
