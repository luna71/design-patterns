package Creational.AbstractFactoryPattern;

import java.awt.*;

public class LightUIFactory implements UIFactory{
    @Override
    public Button createButton() {
        Button uiButton = new UIButton();
        uiButton.SetColor(Color.WHITE);
        return uiButton;
    }
}
