package Creational.AbstractFactoryPattern;

import java.awt.*;

public interface Button {
    void Click();
    void SetColor(Color color);
}
