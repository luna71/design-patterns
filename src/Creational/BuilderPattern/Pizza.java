package Creational.BuilderPattern;

public interface Pizza {

    public void Cook();
    public void Eat();

}
