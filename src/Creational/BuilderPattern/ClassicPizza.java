package Creational.BuilderPattern;

public class ClassicPizza implements Pizza {

    private Builder.BaseTypeEnum baseType;

    private Builder.SauceEnum sauceType;

    private Boolean cheese;

    public static class Builder {
        public Builder(){}

        public enum BaseTypeEnum {
            Thin,
            Deep,
            Italian,
        }

        public enum SauceEnum {
            Tomato,
            Barbeque,
        }

        private BaseTypeEnum baseType;

        private SauceEnum sauceType;

        private Boolean cheese;

        public Builder selectBase(BaseTypeEnum base) {
            this.baseType = base;
            return this;
        }

        public Builder selectSauce(SauceEnum sauce) {
            this.sauceType = sauce;
            return this;
        }

        public Builder addCheese(Boolean cheese) {
            this.cheese = cheese;
            return this;
        }

        public ClassicPizza build() {
            ClassicPizza classicPizza = new ClassicPizza();
            classicPizza.baseType = this.baseType;
            classicPizza.sauceType = this.sauceType;
            classicPizza.cheese = this.cheese;
            return classicPizza;
        }

    }


    @Override
    public void Cook() {
        System.out.println("Cooks ClassicPizza");
    }

    @Override
    public void Eat() {
        System.out.println("Eats ClassicPizza");
    }
}
