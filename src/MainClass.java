import Structural.CompositePattern.File;
import Structural.CompositePattern.Folder;

public class MainClass {

    public static void main(String[] args) {
        Folder folder1 = new Folder();
        Folder folder2 = new Folder();

        File file1 = new File();
        File file2 = new File();
        File file3 = new File();


        folder1.addChild(file1);
        folder1.addChild(file2);
        folder2.addChild(file3);

        folder1.addChild(folder2);

        folder1.Print();
    }

}
