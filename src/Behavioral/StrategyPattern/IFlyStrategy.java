package Behavioral.StrategyPattern;

public interface IFlyStrategy {
    void Fly();
}
