package Behavioral.StrategyPattern;

public class Bird {

    private IFlyStrategy flyStrategy;

    public Bird (IFlyStrategy flyStrategy) {
        this.flyStrategy = flyStrategy;
    }

    public void Fly() {
        flyStrategy.Fly();
    }

}
