package Behavioral.StrategyPattern;

public class BirdFactory {

    public static Bird makeBird() {
        return new Bird(new NormalFly());
    }

}
