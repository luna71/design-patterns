package Behavioral.ObserverPattern;

import java.util.ArrayList;

public class Subject<T> {

    private T subjectData;

    private ArrayList<Observer> observers = new ArrayList<>();

    public Subject() {

    }

    public Subject(T object) {
        subjectData = object;
    }

    public void observe(Observer observer) {
        observers.add(observer);
    }

    private void notifyObservers() {
        for (Observer observer : observers)
            observer.notifyChange(subjectData);
    }


    public void setData(T data) {
        subjectData = data;
        notifyObservers();
    }

    public T getData() {
        return subjectData;
    }
}
