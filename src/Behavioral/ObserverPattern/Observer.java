package Behavioral.ObserverPattern;

public interface Observer<T> {
    void notifyChange(T change);
}
