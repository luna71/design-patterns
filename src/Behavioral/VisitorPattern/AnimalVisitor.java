package Behavioral.VisitorPattern;

public interface AnimalVisitor {
    void visitDog(Dog dog);
    void visitCat(Cat cat);
}
