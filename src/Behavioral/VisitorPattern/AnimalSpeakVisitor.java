package Behavioral.VisitorPattern;

public class AnimalSpeakVisitor implements AnimalVisitor {
    @Override
    public void visitDog(Dog dog) {
        System.out.println("Behavioral.VisitorPattern.Dog speaks");
    }

    @Override
    public void visitCat(Cat cat) {
        System.out.println("Behavioral.VisitorPattern.Cat speaks");
    }
}
