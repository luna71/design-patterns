package Behavioral.VisitorPattern;

public class Cat extends Animal {

    Cat() {
        super();
        name = "Behavioral.VisitorPattern.Cat";
    }

    @Override
    public void acceptVisitor(AnimalVisitor animalVisitor) {
        animalVisitor.visitCat(this);
    }

}
