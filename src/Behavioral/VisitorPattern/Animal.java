package Behavioral.VisitorPattern;

public abstract class Animal {
    public String name;

    Animal() {
        name = "Behavioral.VisitorPattern.Animal";
    }

    public abstract void acceptVisitor(AnimalVisitor animalVisitor);
}
