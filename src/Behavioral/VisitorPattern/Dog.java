package Behavioral.VisitorPattern;

public class Dog extends Animal {

    Dog() {
        super();
        name = "Behavioral.VisitorPattern.Dog";
    }

    @Override
    public void acceptVisitor(AnimalVisitor animalVisitor) {
        animalVisitor.visitDog(this);
    }
}
