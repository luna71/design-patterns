package Structural.CompositePattern;

public interface Node {
    void Print();
}
