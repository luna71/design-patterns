package Structural.CompositePattern;

import java.util.ArrayList;

public class Folder implements Node {

    ArrayList<Node> childNodes = new ArrayList<>();

    @Override
    public void Print() {
        System.out.println("Folder");
        for (Node node:
             childNodes) {
            node.Print();
        }
    }

    public void addChild(Node node) {
        childNodes.add(node);
    }
}
