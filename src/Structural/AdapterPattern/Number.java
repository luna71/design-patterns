package Structural.AdapterPattern;

public interface Number {
    Integer add(Integer number);
}
