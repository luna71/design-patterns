package Structural.AdapterPattern;

public class NumberImplementation implements Number {

    private Integer Number;

    public NumberImplementation(Integer number) {
        this.Number = number;
    }

    public Integer add(Integer number) {
        return number + this.Number;
    }

}
