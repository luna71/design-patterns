package Structural.AdapterPattern;

public class StringNumberAdapter implements Number {

    private StringNumber stringNum;

    public StringNumberAdapter(StringNumber num) {
        stringNum = num;
    }

    @Override
    public Integer add(Integer number) {
        Integer stringNumberNumber = 0;
        try {
            stringNumberNumber = Integer.valueOf(stringNum.getString());
        }
        catch (Exception e) {
            System.out.println("String in StringNumberAdapter can't convert to a number, using 0 instead.");
        }
        return stringNumberNumber + number;
    }
}
