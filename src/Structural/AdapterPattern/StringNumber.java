package Structural.AdapterPattern;

public class StringNumber {

    private String num;

    public StringNumber(String num) {
        this.num = num;
    }

    public String getString() {
        return num;
    }
}
